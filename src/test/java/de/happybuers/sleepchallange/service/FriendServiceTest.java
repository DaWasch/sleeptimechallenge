package de.happybuers.sleepchallange.service;

import de.happybuers.sleepchallange.model.FriendRequestInput;
import de.happybuers.sleepchallange.persistence.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

@SpringBootTest
@ActiveProfiles("test")
class FriendServiceTest {
    @Autowired
    UserRepo userRepo;
    @Autowired
    FriendRequestRepo requestRepo;
    @Autowired
    FriendshipRepo friendshipRepo;
    @Autowired
    FriendService friendService;
    @MockBean
    EmailService emailService;

    @BeforeEach
    void setup(){
        this.userRepo.deleteAll();
        this.requestRepo.deleteAll();
        this.userRepo.save(UserEntity.builder()
                .name("Test User 1")
                .sub("12345")
                .email("testuser1@gmail.com")
                .authorizedClientRegistrationId("google")
                .build());
        this.userRepo.save(UserEntity.builder()
                .name("Test User 2")
                .sub("23456")
                .email("testuser2@gmail.com")
                .authorizedClientRegistrationId("google")
                .build());
    }

    @Test
    void saveFriendRequest() {
        Optional<UserEntity> user = this.userRepo.findUserEntityByEmail("testuser1@gmail.com");
        FriendRequestInput requestInput = FriendRequestInput.builder()
                .id(user.get().getId())
                .email("testuser2@gmail.com")
                .build();
        this.friendService.saveFriendRequest(requestInput,user.get());

        List< FriendRequest> friendRequests  = this.requestRepo.getFriendRequestByUserAndAcceptedIsFalse(user.get());
        assertEquals(1,friendRequests.size());
        friendRequests = this.requestRepo.getFriendRequestByFriendAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());
        user = this.userRepo.findUserEntityByEmail("testuser2@gmail.com");
        friendRequests = this.requestRepo.getFriendRequestByFriendAndAcceptedIsFalse(user.get());
        assertEquals(1,friendRequests.size());
    }

    @Test
    void deleteRequestByUser() {
        Optional<UserEntity> user = this.userRepo.findUserEntityByEmail("testuser1@gmail.com");
        FriendRequestInput requestInput = FriendRequestInput.builder()
                .id(user.get().getId())
                .email("testuser2@gmail.com")
                .build();
        this.friendService.saveFriendRequest(requestInput,user.get());

        Optional<UserEntity> friend = this.userRepo.findUserEntityByEmail("testuser2@gmail.com");

        this.friendService.deleteRequestByUser(user.get(),friend.get().getId());
        List< FriendRequest> friendRequests  = this.requestRepo.getFriendRequestByUserAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());
        friendRequests = this.requestRepo.getFriendRequestByFriendAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());
        user = this.userRepo.findUserEntityByEmail("testuser2@gmail.com");
        friendRequests = this.requestRepo.getFriendRequestByFriendAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());
    }

    @Test
    void deleteRequestByFriend() {
        Optional<UserEntity> user = this.userRepo.findUserEntityByEmail("testuser1@gmail.com");
        FriendRequestInput requestInput = FriendRequestInput.builder()
                .id(user.get().getId())
                .email("testuser2@gmail.com")
                .build();
        this.friendService.saveFriendRequest(requestInput,user.get());

        Optional<UserEntity> friend = this.userRepo.findUserEntityByEmail("testuser2@gmail.com");

        this.friendService.deleteRequestByFriend(friend.get(),user.get().getId());
        List< FriendRequest> friendRequests  = this.requestRepo.getFriendRequestByUserAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());
        friendRequests = this.requestRepo.getFriendRequestByFriendAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());
        user = this.userRepo.findUserEntityByEmail("testuser2@gmail.com");
        friendRequests = this.requestRepo.getFriendRequestByFriendAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());
    }

    @Test
    void acceptFriendRequest() {
        Optional<UserEntity> user = this.userRepo.findUserEntityByEmail("testuser1@gmail.com");
        FriendRequestInput requestInput = FriendRequestInput.builder()
                .id(user.get().getId())
                .email("testuser2@gmail.com")
                .build();
        /*create a new friend request */
        this.friendService.saveFriendRequest(requestInput,user.get());

        Optional<UserEntity> friend = this.userRepo.findUserEntityByEmail("testuser2@gmail.com");
        /*friend accepts request */
        this.friendService.acceptFriendRequest(friend.get(),user.get().getId());

        /* no unaccepted request should be found*/
        List< FriendRequest> friendRequests  = this.requestRepo.getFriendRequestByUserAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());
        friendRequests  = this.requestRepo.getFriendRequestByFriendAndAcceptedIsFalse(user.get());
        assertTrue(friendRequests.isEmpty());

        /*a new friendship should exists friends see each other */
        List<UserEntity> friends = this.friendshipRepo.getFriendsByUser(user.get());

        assertEquals(1, friends.size());

        assertEquals(friend.get().getId(),friends.get(0).getId());

        friends = this.friendshipRepo.getFriendsByUser(friend.get());

        assertEquals(1, friends.size());

        assertEquals(user.get().getId(),friends.get(0).getId());


    }

    @Test
    void deleteFriend() {
        Optional<UserEntity> user = this.userRepo.findUserEntityByEmail("testuser1@gmail.com");
        FriendRequestInput requestInput = FriendRequestInput.builder()
                .id(user.get().getId())
                .email("testuser2@gmail.com")
                .build();
        /*create a new friend request */
        this.friendService.saveFriendRequest(requestInput,user.get());

        Optional<UserEntity> friend = this.userRepo.findUserEntityByEmail("testuser2@gmail.com");
        /*friend accepts request */
        this.friendService.acceptFriendRequest(friend.get(),user.get().getId());

        /*a new friendship should exists friends see each other */
        List<UserEntity> friends = this.friendshipRepo.getFriendsByUser(user.get());
        assertEquals(1, friends.size());

        /* delete  friendship */
        this.friendService.deleteFriend(user.get(),friend.get().getId());

        friends = this.friendshipRepo.getFriendsByUser(user.get());
        assertTrue(friends.isEmpty());

        friends = this.friendshipRepo.getFriendsByUser(friend.get());
        assertTrue(friends.isEmpty());
    }
}