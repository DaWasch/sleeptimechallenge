package de.happybuers.sleepchallange.service;

import de.happybuers.sleepchallange.model.UserInputs;
import de.happybuers.sleepchallange.persistence.UserEntity;
import de.happybuers.sleepchallange.persistence.UserRepo;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

@SpringBootTest
@ActiveProfiles("test")
class UserServiceTest {
    @Autowired
    UserService userService;
    @Autowired
    UserRepo userRepo;
    @MockBean
    EmailService emailService;
    @Test
    void saveUser() {
        this.userRepo.deleteAll();
        Long id = this.userService.saveUser(UserInputs.builder().name("testUser").email("testMail").sub("1234").authorizedClientRegistrationId("google").build());
        UserEntity user = this.userRepo.getUserEntityById(id);
        assertEquals("testUser",user.getName());
        assertEquals("testMail",user.getEmail());
    }

    @Test
    void findAll() {
        this.userRepo.deleteAll();
        this.userService.saveUser(UserInputs.builder().name("testUser1").email("testMail1").sub("1").authorizedClientRegistrationId("google").build());
        this.userService.saveUser(UserInputs.builder().name("testUser2").email("testMail2").sub("2").authorizedClientRegistrationId("google").build());
        this.userService.saveUser(UserInputs.builder().name("testUser3").email("testMail3").sub("3").authorizedClientRegistrationId("google").build());
        this.userService.saveUser(UserInputs.builder().name("testUser4").email("testMail4").sub("4").authorizedClientRegistrationId("google").build());

        List<UserEntity> userEntities = this.userService.findAll();

        assertEquals(4, userEntities.size());
    }

    @Test
    void getUserById() {
        this.userRepo.deleteAll();
        Long id = this.userService.saveUser(UserInputs.builder().name("testUser1").email("testMail1").sub("1234").authorizedClientRegistrationId("google").build());

        UserEntity user = this.userService.getUserById(id);
        assertEquals("testUser1",user.getName());

    }
}