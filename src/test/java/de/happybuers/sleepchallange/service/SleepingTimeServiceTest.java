package de.happybuers.sleepchallange.service;

import de.happybuers.sleepchallange.model.UserInputs;
import de.happybuers.sleepchallange.persistence.SleepingTimeEntity;
import de.happybuers.sleepchallange.persistence.SleepingTimeRepo;
import de.happybuers.sleepchallange.persistence.UserEntity;
import de.happybuers.sleepchallange.persistence.UserRepo;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
@ActiveProfiles("test")
class SleepingTimeServiceTest {

    @Autowired
    SleepingTimeService sleepingTimeService;
    @Autowired
    SleepingTimeRepo sleepingTimeRepo;
    @Autowired
    UserService userService;
    @Autowired
    UserRepo userRepo;
    @MockBean
    EmailService emailService;
    @BeforeEach
    void setup(){
        this.userRepo.deleteAll();
        this.userService.saveUser(UserInputs.builder().name("testUser1").email("testMail1").sub("1").authorizedClientRegistrationId("google").build());
        this.userService.saveUser(UserInputs.builder().name("testUser2").email("testMail2").sub("2").authorizedClientRegistrationId("google").build());
    }
    @Test
    void getSleepingTimeByUserName() {
        this.userService.findAll().forEach(u -> this.sleepingTimeService.saveSleepingTime(u.getId(), LocalDateTime.now(),null));
        List<SleepingTimeEntity> sleepingTimeEntities = this.sleepingTimeService.getSleepingTimeByUserName("testUser1");
        assertEquals(1,sleepingTimeEntities.size());
    }

    @Test
    void saveSleepingTime() {
        this.userService.findAll().forEach(u -> this.sleepingTimeService.saveSleepingTime(u.getId(), LocalDateTime.now(),null));
        List<SleepingTimeEntity> sleepingTimeList = this.sleepingTimeRepo.findAll();
        assertEquals(2,sleepingTimeList.size());
    }

    @Test
    void getSumOfPoints() {
        UserEntity user = this.userRepo.findUserEntityByName("testUser1").orElseThrow();
        this.sleepingTimeService.saveSleepingTime(user.getId(), LocalDateTime.of(2023,1,1,22,0),null);
        this.sleepingTimeService.saveSleepingTime(user.getId(), LocalDateTime.of(2023,1,2,23,0),null);
        user = this.userRepo.findUserEntityByName("testUser1").orElseThrow();
        assertEquals(5,this.sleepingTimeService.getSumOfPoints(user.getSleepingTimeSet()));
    }

    @Test
    void getPointsForTime() {
        assertEquals(3, this.sleepingTimeService.getPointsForTime(LocalDateTime.of(2023,1,1,21,45)));
        assertEquals(3, this.sleepingTimeService.getPointsForTime(LocalDateTime.of(2023,1,1,22,0)));
        assertEquals(2, this.sleepingTimeService.getPointsForTime(LocalDateTime.of(2023,1,1,22,45)));
        assertEquals(2, this.sleepingTimeService.getPointsForTime(LocalDateTime.of(2023,1,1,23,0)));
        assertEquals(1, this.sleepingTimeService.getPointsForTime(LocalDateTime.of(2023,1,1,23,45)));
        assertEquals(1, this.sleepingTimeService.getPointsForTime(LocalDateTime.of(2023,1,1,0,0)));
        assertEquals(0, this.sleepingTimeService.getPointsForTime(LocalDateTime.of(2023,1,1,1,0)));
    }
}