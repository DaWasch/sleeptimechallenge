package de.happybuers.sleepchallange;

import de.happybuers.sleepchallange.service.EmailService;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.ActiveProfiles;

@SpringBootTest
@ActiveProfiles("test")
class SleepchallangeApplicationTests {

	@MockBean
	EmailService emailService;
	@Test
	void contextLoads() {
	}

}
