package de.happybuers.sleepchallange.error;

public class MailAlreadyRegisteredException extends Exception{
    public MailAlreadyRegisteredException(String mail){
        super("Mail " + mail + " already in use");
    }
}
