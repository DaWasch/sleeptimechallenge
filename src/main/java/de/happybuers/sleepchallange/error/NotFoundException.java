package de.happybuers.sleepchallange.error;

public class NotFoundException extends Exception{
    public NotFoundException(String msg){
        super("NotFoundException " + msg);
    }
}
