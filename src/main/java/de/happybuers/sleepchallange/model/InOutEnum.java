package de.happybuers.sleepchallange.model;

public enum InOutEnum {
    IN,
    OUT;
}
