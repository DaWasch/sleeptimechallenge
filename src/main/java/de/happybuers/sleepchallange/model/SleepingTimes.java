package de.happybuers.sleepchallange.model;

import lombok.NoArgsConstructor;

import java.time.LocalDateTime;
@NoArgsConstructor
public class SleepingTimes {
    public static final LocalDateTime ld18 = LocalDateTime.of(1,1,1, 18, 0);
    public static final LocalDateTime ld22 = LocalDateTime.of(1,1,1, 22, 0);
    public static final LocalDateTime ld23 = LocalDateTime.of(1,1,1, 23, 0);
    public static final LocalDateTime ld24 = LocalDateTime.of(1,1,2, 0, 0);
}
