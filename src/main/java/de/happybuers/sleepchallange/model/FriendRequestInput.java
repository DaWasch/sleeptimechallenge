package de.happybuers.sleepchallange.model;

import lombok.*;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class FriendRequestInput {
    Long id;
    String email;
    InOutEnum inOut;

}
