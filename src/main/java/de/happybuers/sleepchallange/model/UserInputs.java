package de.happybuers.sleepchallange.model;

import lombok.*;


@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class UserInputs {
    String name;
    String email;
    String authorizedClientRegistrationId;
    String sub;
}
