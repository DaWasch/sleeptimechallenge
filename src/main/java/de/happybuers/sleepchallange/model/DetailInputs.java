package de.happybuers.sleepchallange.model;

import lombok.*;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDate;

@ToString
@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Builder
public class DetailInputs {
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate day;
    private int hour;
    private int minute;
    private Long userId;
}
