package de.happybuers.sleepchallange.model;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
public class TimeSpan {
    private String display;
    private Integer points;
}
