package de.happybuers.sleepchallange.controller;

import de.happybuers.sleepchallange.persistence.UserEntity;
import de.happybuers.sleepchallange.persistence.UserRepo;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController()
@RequestMapping("/api/user")
public class UserController {
    private final UserRepo userRepo;

    public UserController(UserRepo userRepo) {
        this.userRepo = userRepo;
    }

    @GetMapping("/{name}")
    public ResponseEntity<UserEntity> getUserByName(@PathVariable("name")String name){
        Optional<UserEntity> userEntity = this.userRepo.findUserEntityByName(name);
        if(userEntity.isPresent()){
            return new ResponseEntity<>(userEntity.get(), HttpStatus.OK);
        }
        return new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

//    @PostMapping("/save")
//    public ResponseEntity<Long> saveUser(@RequestParam("name") String name, @RequestParam("email") String email){
//        return new ResponseEntity<>(this.userService.saveUser(name, email), HttpStatus.OK);
//    }
    @GetMapping("/all")
    public ResponseEntity<List<UserEntity>> getAllUsers(){
        return new ResponseEntity<>(this.userRepo.findAll(),HttpStatus.OK);
    }
}
