package de.happybuers.sleepchallange.controller;

import de.happybuers.sleepchallange.persistence.SleepingTimeEntity;
import de.happybuers.sleepchallange.service.SleepingTimeService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;

@RestController
@RequestMapping("/api/sleep")
public class SleepingTimeController {
    private final SleepingTimeService sleepingTimeService;
    public SleepingTimeController(SleepingTimeService sleepingTimeService) {
        this.sleepingTimeService = sleepingTimeService;
    }

    @GetMapping("/{user}")
    public ResponseEntity<List<SleepingTimeEntity>> getSleepingTimeOfUser(@PathVariable String user){
        return new ResponseEntity<>(this.sleepingTimeService.getSleepingTimeByUserName(user), HttpStatus.OK);
    }
    @PostMapping("/save")
    public ResponseEntity<Long> saveSleepingTime(@RequestParam Long id
            , @RequestParam(name = "goToBedTime",required = false) LocalDateTime goToBedTime
            , @RequestParam(name = "wakeTime",required = false) LocalDateTime wakeTime
    ){
        return  new ResponseEntity<>(this.sleepingTimeService.saveSleepingTime(id,goToBedTime,wakeTime),HttpStatus.OK);
    }
}
