package de.happybuers.sleepchallange.controller;

import de.happybuers.sleepchallange.error.NotFoundException;
import de.happybuers.sleepchallange.model.*;
import de.happybuers.sleepchallange.persistence.*;
import de.happybuers.sleepchallange.service.FriendService;
import de.happybuers.sleepchallange.service.SleepingTimeService;
import de.happybuers.sleepchallange.service.UserService;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.time.LocalDateTime;
import java.util.*;
import java.util.stream.Collectors;

@Controller
@RequestMapping("/")
public class ViewController {
    @Value("${base.url}")
    String baseUrl;
    private final UserService userService;
    private final SleepingTimeService sleepingTimeService;
    private final SleepingTimeRepo sleepingTimeRepo;
    private final UserRepo userRepo;
    private final FriendService friendService;
    private final FriendshipRepo friendshipRepo;

    public ViewController(UserService userService, SleepingTimeService sleepingTimeService, SleepingTimeRepo sleepingTimeRepo
            , UserRepo userRepo, FriendService friendService, FriendshipRepo friendshipRepo) {
        this.userService = userService;
        this.sleepingTimeService = sleepingTimeService;
        this.sleepingTimeRepo = sleepingTimeRepo;
        this.userRepo = userRepo;
        this.friendService = friendService;
        this.friendshipRepo = friendshipRepo;
    }

    @GetMapping
    public String users(Model model, OAuth2AuthenticationToken authentication) {
        UserEntity user = this.userService.getUserByAuth(authentication);
        if(user == null){
            return "redirect:welcome";
        }
        /*Find all friends*/
        List<UserEntity> allFriends = this.friendshipRepo.getFriendsByUser(user).stream().collect(Collectors.toList());
        /*add details (points)*/
        allFriends.forEach(f -> {
            try {
                this.friendService.addFriendsDetails(user,f);
            } catch (NotFoundException e) {
                throw new RuntimeException(e);
            }
        });
        model.addAttribute("baseUrl",this.baseUrl);
        model.addAttribute("currentUser",user);
        model.addAttribute("friends", allFriends.stream().sorted(Comparator.comparing(UserEntity::getSumOfPoints).reversed()).toList());
        model.addAttribute("friendrequests",user.friendRequestsSent.stream().filter(r -> !r.accepted).toList()
                .stream().map(r -> r.friend).toList());
        model.addAttribute("incomingrequests",user.friendRequestsReceived.stream().filter(r -> !r.accepted).toList()
                .stream().map(r -> r.user).toList());
        model.addAttribute("friendrequest",new FriendRequestInput());
        model.addAttribute("deleterequestout",new FriendRequestInput());

        return "index";
    }
    @PostMapping("/addFriend")
    private String addFriend(FriendRequestInput input, OAuth2AuthenticationToken authentication){
        UserEntity user = this.userService.getUserByAuth(authentication);
        this.friendService.saveFriendRequest(input,user);
        return "redirect:/";
    }
    @PostMapping("/deleteFriend")
    private String deleteFriend(FriendRequestInput input, OAuth2AuthenticationToken authentication){
        UserEntity user = this.userService.getUserByAuth(authentication);
        this.friendService.deleteFriend(user, input.getId());
        return "redirect:/";
    }
    @GetMapping("/delete")
    private String delete(OAuth2AuthenticationToken authentication){
        UserEntity user = this.userService.getUserByAuth(authentication);
        this.userService.deleteUser(user);
        return "redirect:welcome";
    }
    @PostMapping("/deleteFriendRequest")
    private String deleteFriendRequest(FriendRequestInput friendRequestInput, OAuth2AuthenticationToken authentication){
        UserEntity user = this.userService.getUserByAuth(authentication);
        if(friendRequestInput.getInOut().equals(InOutEnum.OUT)) {
            this.friendService.deleteRequestByUser(user, friendRequestInput.getId());
        }else{
            this.friendService.deleteRequestByFriend(user, friendRequestInput.getId());
        }
        return "redirect:/";
    }

    @PostMapping("/acceptFriendRequest")
    private String acceptFriendRequest(FriendRequestInput input, OAuth2AuthenticationToken authentication){
        UserEntity user = this.userService.getUserByAuth(authentication);
        this.friendService.acceptFriendRequest(user, input.getId());
        return "redirect:/";
    }

    @GetMapping("/detail")
    public String details(Model model, OAuth2AuthenticationToken authentication){
        UserEntity user = this.userService.getUserByAuth(authentication);
        LocalDateTime now = LocalDateTime.now();
        DetailInputs detailInputs = DetailInputs.builder()
                .day(now.toLocalDate())
                .hour(now.getHour())
                .minute(now.getMinute())
                .userId(user.getId())
                .build();

        model.addAttribute("baseUrl",this.baseUrl);
        model.addAttribute("user",user);
        model.addAttribute("detailinputs", detailInputs);
        List<TimeSpan> timeSpans = List.of(new TimeSpan(SleepingTimes.ld18.getHour() + " -> " + SleepingTimes.ld22.getHour(),3)
                ,new TimeSpan(SleepingTimes.ld22.getHour() + " -> " + SleepingTimes.ld23.getHour(),2)
                ,new TimeSpan(SleepingTimes.ld23.getHour() + " -> " + SleepingTimes.ld24.getHour(),1)
        );
        model.addAttribute("timespans", timeSpans);

        return "detail";
    }
    @PostMapping("/detail")
    public String saveNewBedTime(DetailInputs inputs, Model model){
        UserEntity user = this.userService.getUserById(inputs.getUserId());
        LocalDateTime ld = LocalDateTime.of(inputs.getDay().getYear(),inputs.getDay().getMonth(),inputs.getDay().getDayOfMonth(),inputs.getHour(), inputs.getMinute());
        this.sleepingTimeService.saveSleepingTime(user.getId(),ld,null);

        user = this.userService.getUserById(user.getId());
        model.addAttribute("user",user);
        model.addAttribute("detailinputs", inputs);
        return "redirect:detail?id=" + user.getId();
    }
    @GetMapping("/deletetime")
    public String deleteSleepingTime(@RequestParam Long id){
        SleepingTimeEntity sleepingTime = this.sleepingTimeRepo.getSleepingTimeEntityById(id);
        this.sleepingTimeRepo.delete(sleepingTime);
        return "redirect:detail?id=" + sleepingTime.user.getId();
    }
    @GetMapping("/deleteuser")
    public String deleteUser(@RequestParam Long id){
        this.userRepo.delete(this.userService.getUserById(id));
        return "redirect:/";
    }

    @GetMapping("/loginSuccess")
    public String getLoginInfo(OAuth2AuthenticationToken authentication) {
        Map<String,Object> userAttributes = authentication.getPrincipal().getAttributes();

        if(userAttributes.get("email") != null) {
            UserEntity user = this.userService.getUserByAuth(authentication);
            if(user != null) {
                return "redirect:/";
            }
        }

        return "redirect:welcome";
    }

    @GetMapping("/welcome")
    public String getWelcome(Model model, OAuth2AuthenticationToken authentication){
        Map<String,Object> userAttributes = authentication.getPrincipal().getAttributes();
        /* try to get given_name else name */
        String name = (String)userAttributes.get("given_name");
        if (name == null) {
            name = (String)userAttributes.get("name");
        }
        UserInputs user = UserInputs.builder()
                .name(name)
                .sub((String)userAttributes.get("sub"))
                .email((String) userAttributes.get("email"))
                .authorizedClientRegistrationId(authentication.getAuthorizedClientRegistrationId())
                .build();
        if(user.getSub() == null || user.getSub().isEmpty()){
            user.setSub((String) userAttributes.get("id"));
        }
        model.addAttribute("baseUrl",this.baseUrl);
        model.addAttribute("user",user);
        return "welcome";
    }

    @GetMapping("/error")
    private String showError(){
        return "error";
    }

    @PostMapping("/register")
    public String saveNewUser(UserInputs userInputs, Model model){
        this.userService.saveUser(userInputs);
        return "redirect:/";
    }
}
