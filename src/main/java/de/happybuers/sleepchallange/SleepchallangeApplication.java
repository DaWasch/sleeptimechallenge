package de.happybuers.sleepchallange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SleepchallangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(SleepchallangeApplication.class, args);
	}

}
