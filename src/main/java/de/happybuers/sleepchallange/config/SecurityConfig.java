package de.happybuers.sleepchallange.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SecurityConfig {

    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests(requests -> requests
                .requestMatchers("/oauth_login", "/static/**", "/webjars/**")
                .permitAll()
                .anyRequest()
                .authenticated())
                .oauth2Login(login -> login
                        .loginPage("/oauth_login")
                        .defaultSuccessUrl("/loginSuccess", true)
                        .failureUrl("/loginFailure"));;
        return http.build();
    }
}
