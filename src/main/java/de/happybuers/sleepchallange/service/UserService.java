package de.happybuers.sleepchallange.service;

import de.happybuers.sleepchallange.model.UserInputs;
import de.happybuers.sleepchallange.persistence.SleepingTimeEntity;
import de.happybuers.sleepchallange.persistence.UserEntity;
import de.happybuers.sleepchallange.persistence.UserRepo;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Service
public class UserService {
    private final UserRepo userRepo;
    private final SleepingTimeService sleepingTimeService;
    private final OAuth2AuthorizedClientService authService;
    private final EmailService emailService;

    private final String SUBJECT_PREFIX = "SleepTimeChallenge: ";
    @Value("${base.url}")
    public String base_url;
    public UserService(UserRepo userRepo, SleepingTimeService sleepingTimeService, OAuth2AuthorizedClientService authService, EmailService emailService) {
        this.userRepo = userRepo;
        this.sleepingTimeService = sleepingTimeService;
        this.authService = authService;
        this.emailService = emailService;
    }

    public Long saveUser(UserInputs userInputs){
        Optional<UserEntity> user = this.userRepo.findUserEntityByAuthorizedClientRegistrationIdAndSub(userInputs.getAuthorizedClientRegistrationId(),userInputs.getSub());

        if(user.isPresent()){
            if(!user.get().getEmail().equals(userInputs.getEmail())){
                throw new RuntimeException("existing user with foreign mail address");
            }
            return user.get().getId();
        }

        if(userInputs.getSub() == null || userInputs.getSub().isEmpty()
                ||userInputs.getAuthorizedClientRegistrationId() == null
                ||userInputs.getAuthorizedClientRegistrationId().isEmpty()
        ){
            throw new RuntimeException("Regsitration info not complete");
        }


        UserEntity userEntity = UserEntity.builder()
                .name(userInputs.getName())
                .email(userInputs.getEmail())
                .sub(userInputs.getSub())
                .authorizedClientRegistrationId(userInputs.getAuthorizedClientRegistrationId())
                .build();
        this.userRepo.save(userEntity);
        /*send mail to user*/
        this.emailService.sendSimpleMessage(userEntity.getEmail(),SUBJECT_PREFIX + " Registrierung erfolgreich!",
                "Hallo " + userEntity.getName() +",\n" +
                        "du hast dich erfolgreich bei der Sleep Time Challenge registriert mit deiner Email " + userEntity.getEmail() + " !\n\n" +
                        "Wie es weiter geht, erfährst du unter: \n\n" + this.base_url + "\n\n" +
                        "Viel Spaß!"
        );
        return userEntity.getId();
    }

    public void deleteUser(UserEntity userEntity){
        this.userRepo.delete(userEntity);
        /*send mail to user*/
        this.emailService.sendSimpleMessage(userEntity.getEmail(),SUBJECT_PREFIX + " Löschung erfolgreich :(",
                "Hallo " + userEntity.getName() +",\n" +
                        "schade, dass du nicht mehr mitmachen möchtest. Alle deine Daten wurden gelöscht!\n\n" +
                        "Wenn du es noch einmal probieren möchtest, schaue gern vorbei: \n\n" + this.base_url + "\n\n" +
                        "Viel Spaß!"
        );
    }

    public List<UserEntity> findAll(){
        List<UserEntity> userEntities = this.userRepo.findAll();
        if(!userEntities.isEmpty()){
            userEntities.forEach(u -> {
                        u.setSumOfPoints(this.sleepingTimeService.getSumOfPoints(u.getSleepingTimeSet()));
                        u.setSleepingTimeList(u.getSleepingTimeSet().stream().sorted(Comparator.comparing(s -> s.goToBedTime)).toList());
                    }
            );
        }
        return userEntities;
    }
    public UserEntity getUserById(Long id){
        UserEntity user = this.userRepo.getUserEntityById(id);
        addUserDetails(user);
        return user;
    }
    public UserEntity getUserByMail(String email){
        UserEntity user = this.userRepo.findUserEntityByEmail(email).orElse(null);
        addUserDetails(user);
        return user;
    }

    private UserEntity getUserByAuthClientIdAndId(String id, String sub){
        UserEntity user = this.userRepo.findUserEntityByAuthorizedClientRegistrationIdAndSub(id,sub).orElse(null);
        addUserDetails(user);
        return user;
    }

    private Map<String,Object> getAuthProperties(OAuth2AuthenticationToken authentication){
        String authProvider = authentication.getAuthorizedClientRegistrationId();
        /*get Auth Client Provider*/
        OAuth2AuthorizedClient client = this.authService
                .loadAuthorizedClient(
                        authProvider,
                        authentication.getName());
        /*get Endpoint*/
        String userInfoEndpointUri = client.getClientRegistration()
                .getProviderDetails().getUserInfoEndpoint().getUri();
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.AUTHORIZATION, "Bearer " + client.getAccessToken()
                .getTokenValue());
        HttpEntity entity = new HttpEntity("", headers);
        ResponseEntity<Map> response = restTemplate
                .exchange(userInfoEndpointUri, HttpMethod.GET, entity, Map.class);
        Map userAttributes = response.getBody();
        return userAttributes;
    }
    public UserEntity getUserByAuth(OAuth2AuthenticationToken authentication){
        String authProvider = authentication.getAuthorizedClientRegistrationId();
        Map userAttributes = this.getAuthProperties(authentication);
        /* Google delivers sub as ID */
        String id = (String) userAttributes.get("sub");
        if(id == null || id.isEmpty()){
            id = (String) userAttributes.get("id");
        }
        return this.getUserByAuthClientIdAndId(authProvider,id);
    }

    public void addUserDetails(UserEntity user){
        if(user != null){
            user.setSumOfPoints(this.sleepingTimeService.getSumOfPoints(user.getSleepingTimeSet()));
            user.setSleepingTimeList(user.getSleepingTimeSet().stream().sorted(Comparator.comparing(SleepingTimeEntity::getGoToBedTime, Comparator.nullsLast(Comparator.reverseOrder()))).toList());
        }
    }
}
