package de.happybuers.sleepchallange.service;

import de.happybuers.sleepchallange.error.NotFoundException;
import de.happybuers.sleepchallange.model.FriendRequestInput;
import de.happybuers.sleepchallange.persistence.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@Slf4j
public class FriendService {
    private final FriendshipRepo friendshipRepo;
    private final User2FriendshipRepo user2FriendshipRepo;
    private final FriendRequestRepo friendRequestRepo;
    private final UserRepo userRepo;
    private final SleepingTimeService sleepingTimeService;
    private final EmailService emailService;

    private final String SUBJECT_PREFIX = "SleepTimeChallenge: ";
    @Value("${base.url}")
    public String base_url;

    public FriendService(FriendshipRepo friendshipRepo, User2FriendshipRepo user2FriendshipRepo, FriendRequestRepo friendRequestRepo, UserRepo userRepo, SleepingTimeService sleepingTimeService, EmailService emailService) {
        this.friendshipRepo = friendshipRepo;
        this.user2FriendshipRepo = user2FriendshipRepo;
        this.friendRequestRepo = friendRequestRepo;
        this.userRepo = userRepo;
        this.sleepingTimeService = sleepingTimeService;
        this.emailService = emailService;
    }

    public Boolean saveFriendRequest(FriendRequestInput friendRequestInput, UserEntity user){
        Optional<UserEntity> friend = this.userRepo.findUserEntityByEmail(friendRequestInput.getEmail());
        if(friend.isPresent()){
            log.info("saving FriendRequest User " + user.getId() + " Friend " + friend.get().getId());
            List<FriendRequest> oldRequests = this.friendRequestRepo.findByUserAndFriendAndAcceptedFalse(user,friend.get());
            /* if request already exists don't add a new one*/
            if(!oldRequests.isEmpty()){
                return true;
            }
            FriendRequest friendRequest = FriendRequest.builder()
                    .accepted(false)
                    .friend(friend.get())
                    .user(user)
                    .build();
            this.friendRequestRepo.save(friendRequest);
            log.info("FriendRequest saved User " + user.getId() + " Friend " + friendRequestInput.getId());
            this.emailService.sendSimpleMessage(friendRequestInput.getEmail(),SUBJECT_PREFIX + " Freundschaftsanfrage von " + user.getName(),
                    "Hallo " + friend.get().getName() +",\n" +
                            "du hast eine Freundschaftsanfrage von "+ user.getName() + " (" + user.getEmail() + ") für die Sleep Time Challlenge erhalten!\n\n" +
                            "bitte bestätige die Anfrage unter " + this.base_url + "\n\n" +
                            "Viele Grüße!"
                    );
            return true;
        }
        log.info("FriendRequest not saved Friend " + friendRequestInput.getId() +  " not found");
        return false;
    }

    public void deleteRequestByUser(UserEntity user, Long friendId){
        UserEntity friend  = this.userRepo.getUserEntityById(friendId);
        log.info("delete Friend Request by User " + user.getId() + " Friend " + friendId);
        this.friendRequestRepo.deleteAll(this.friendRequestRepo.findByUserAndFriendAndAcceptedFalse(user,friend));
    }
    public void deleteRequestByFriend(UserEntity user, Long friendId){
        UserEntity friend  = this.userRepo.getUserEntityById(friendId);
        log.info("delete Friend Request by Friend " + friendId + " User " + user.getId());
        this.friendRequestRepo.deleteAll(this.friendRequestRepo.findByUserAndFriendAndAcceptedFalse(friend,user));
    }

    public void acceptFriendRequest(UserEntity user, Long friendId){
        UserEntity friendUser  = this.userRepo.getUserEntityById(friendId);
        log.info("accepting Friend Request " + user.getId() + " Friend " + friendId);
        Optional<UserEntity> friend = this.friendshipRepo.findFriendsByUserAndFriend(user,friendUser);

        /*only if no friend*/
        if(friend == null || !friend.isPresent()) {
            /*create new Friendship*/
            Friendship friendship = Friendship.builder()
                    .build();
            this.friendshipRepo.save(friendship);
            User2Friendship friendOfUser = User2Friendship.builder()
                    .friendship(friendship)
                    .user(friendUser)
                    .build();
            this.user2FriendshipRepo.save(friendOfUser);

            User2Friendship user2Friendship = User2Friendship.builder()
                    .friendship(friendship)
                    .user(user)
                    .build();
            this.user2FriendshipRepo.save(user2Friendship);

            /* Search requests by friends for user. should be just one and accept */
            List<FriendRequest> friendRequests = this.friendRequestRepo.findByUserAndFriendAndAcceptedFalse(friendUser,user);

            friendRequests.forEach(r -> r.accepted = true);
            /*send mail to user*/
            this.emailService.sendSimpleMessage(friendUser.getEmail(),SUBJECT_PREFIX + " Freundschaftsanfrage von " + user.getName(),
                    "Hallo " + friendUser.getName() +",\n" +
                            "deine Freundschaftsanfrage an "+ user.getName() + " (" + user.getEmail() + ") für die Sleep Time Challlenge wurde bestätigt!\n\n" +
                            "Viel Spaß bei eurer Challenge auf \n\n" + this.base_url + "\n\n"
            );

            this.friendRequestRepo.saveAll(friendRequests);
        }

    }

    public void deleteFriend(UserEntity user, Long friendId){
        UserEntity friendUser  = this.userRepo.getUserEntityById(friendId);
        Optional<Friendship> friendship = this.friendshipRepo.findFriendShipByUserAndFriend(user,friendUser);
        if(friendship.isPresent()){
            log.info("delete friendship User " + user.getId() + " Friend " + friendId);
            this.friendshipRepo.delete(friendship.get());
            this.deleteAllRequests(user,this.userRepo.getUserEntityById(friendId));
        }
    }

    private void deleteAllRequests(UserEntity user, UserEntity friend){
        log.info("delete friendrequests User " + user.getId() + " Friend " + friend.getId());
        List<FriendRequest> oldRequests = this.friendRequestRepo.findByUserAndFriend(user,friend);
        this.friendRequestRepo.deleteAll(oldRequests);
        oldRequests = this.friendRequestRepo.findByUserAndFriend(friend,user);
        this.friendRequestRepo.deleteAll(oldRequests);
    }

    private int getComparedPointsToFriend(UserEntity user, UserEntity friend) throws NotFoundException {
        log.info("getComparedPointsToFriend User " + user.getId() + " vs friend " + friend.getId());
        Friendship friendship = this.friendshipRepo.findFriendShipByUserAndFriend(user,friend)
                .orElseThrow(() -> new NotFoundException("Friendship between " + user.getEmail() + " and  " + friend.getEmail()));

        return this.sleepingTimeService.getPointsOfUserSinceDate(friend,friendship.startDate)
                - this.sleepingTimeService.getPointsOfUserSinceDate(user,friendship.startDate);
    }

    public void addFriendsDetails(UserEntity user, UserEntity friend) throws NotFoundException {
        friend.setSumOfPoints(this.getComparedPointsToFriend(user,friend));
    }


}
