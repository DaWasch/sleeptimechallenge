package de.happybuers.sleepchallange.service;

import de.happybuers.sleepchallange.model.SleepingTimes;
import de.happybuers.sleepchallange.persistence.SleepingTimeEntity;
import de.happybuers.sleepchallange.persistence.SleepingTimeRepo;
import de.happybuers.sleepchallange.persistence.UserEntity;
import de.happybuers.sleepchallange.persistence.UserRepo;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class SleepingTimeService {
    private final SleepingTimeRepo sleepingTimeRepo;
    private final UserRepo userRepo;

    public SleepingTimeService(SleepingTimeRepo sleepingTimeRepo, UserRepo userRepo) {
        this.sleepingTimeRepo = sleepingTimeRepo;
        this.userRepo = userRepo;
    }

    public List<SleepingTimeEntity> getSleepingTimeByUserName(String name){
        Optional<UserEntity> user = this.userRepo.findUserEntityByName(name);
        if(user.isPresent()){
            return this.sleepingTimeRepo.findByUser(user.get());
        }
        return null;
    }

    public Long saveSleepingTime(Long userId, LocalDateTime goToBedTime, LocalDateTime wakeTime){
        UserEntity userEntity = this.userRepo.getReferenceById(userId);
        LocalDateTime localDateTime = goToBedTime==null?LocalDateTime.now():goToBedTime;

        Optional<SleepingTimeEntity> sleepingTime = this.sleepingTimeRepo
                .findByUserAndGoToBedTimeBetween(userEntity
                        ,localDateTime.minusHours(12)
                        ,localDateTime.plusHours(12)
                );
        if(sleepingTime.isPresent()){
            return sleepingTime.get().id;
        }
        SleepingTimeEntity sleepingTimeEntity = SleepingTimeEntity.builder()
                .goToBedTime(localDateTime)
                .user(userEntity)
                .wakeTime(wakeTime)
                .build();
        this.sleepingTimeRepo.save(sleepingTimeEntity);
        return sleepingTimeEntity.id;
    }

    public int getSumOfPoints(Set<SleepingTimeEntity> sleepingTimeEntities){
        sleepingTimeEntities.forEach(s -> {
            s.points = this.getPointsForTime(s.goToBedTime);
        });
        return sleepingTimeEntities.stream().map(s -> s.points).reduce(0,Integer::sum);
    }

    public int getPointsOfUserSinceDate(UserEntity user, LocalDateTime startDate){
        List<SleepingTimeEntity> sleepingTimeEntityList =  user.getSleepingTimeSet().stream()
                .filter(s -> s.goToBedTime.isAfter(startDate.toLocalDate().atStartOfDay())).toList();
        return sleepingTimeEntityList.stream().map(s -> this.getPointsForTime(s.goToBedTime)).reduce(0,Integer::sum);
    }
    public int getPointsForTime(LocalDateTime bedTime){
        LocalDateTime bedTimeTimeOnly = LocalDateTime.of(1,1,1, bedTime.getHour(), bedTime.getMinute(), bedTime.getSecond());
        if(bedTimeTimeOnly.isAfter(SleepingTimes.ld18) && bedTimeTimeOnly.isBefore(SleepingTimes.ld22) || bedTimeTimeOnly.isEqual(SleepingTimes.ld22)){
            return 3;
        }
        if(bedTimeTimeOnly.isAfter(SleepingTimes.ld22) && bedTimeTimeOnly.isBefore(SleepingTimes.ld23) || bedTimeTimeOnly.isEqual(SleepingTimes.ld23)){
            return 2;
        }
        if(bedTimeTimeOnly.isAfter(SleepingTimes.ld23) && bedTimeTimeOnly.isBefore(SleepingTimes.ld24) || bedTimeTimeOnly.isEqual(SleepingTimes.ld24.minusDays(1))){
            return 1;
        }
        return 0;
    }
}
