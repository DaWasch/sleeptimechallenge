package de.happybuers.sleepchallange.persistence;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;
import java.util.Optional;

public interface FriendshipRepo extends JpaRepository<Friendship,Long> {
    @Query(value = "Select u2f.user" +
            " FROM User2Friendship u2f " +
            " JOIN u2f.friendship fs " +
            "WHERE EXISTS (SELECT u2 " +
            "                FROM User2Friendship u2 " +
            "               WHERE u2.friendship = u2f.friendship" +
            "                 AND u2.user = :user " +
            "                 AND u2.user != u2f.user" +
            "             )")
    List<UserEntity> getFriendsByUser(UserEntity user);
    @Query(value = "Select u2f.user" +
            " FROM User2Friendship u2f " +
            " JOIN u2f.friendship fs " +
            "WHERE EXISTS (SELECT u2 " +
            "                FROM User2Friendship u2 " +
            "               WHERE u2.friendship = u2f.friendship" +
            "                 AND u2.user = :user " +
            "                 AND u2.user != u2f.user" +
            "             )" +
            "  AND u2f.user = :friend")
    Optional<UserEntity> findFriendsByUserAndFriend(UserEntity user, UserEntity friend);

    @Query(value = "Select fs " +
            " FROM Friendship fs " +
            " WHERE EXISTS (SELECT u2f from User2Friendship u2f where u2f.user = :user AND u2f.friendship = fs)" +
            " and EXISTS (SELECT u2f from User2Friendship u2f where u2f.user = :friend AND u2f.friendship = fs)"
    )
    Optional<Friendship> findFriendShipByUserAndFriend(UserEntity user, UserEntity friend);



}
