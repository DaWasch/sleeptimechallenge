package de.happybuers.sleepchallange.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(schema = "sleep",name = "user2friendship_tab")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User2Friendship {
    @Id
    @GeneratedValue
    public Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "frienship_id",referencedColumnName = "id")
    public Friendship friendship;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    public UserEntity user;
    @Builder.Default()
    public LocalDateTime creationTimestamp = LocalDateTime.now();
}
