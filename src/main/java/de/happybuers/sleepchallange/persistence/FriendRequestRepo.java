package de.happybuers.sleepchallange.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FriendRequestRepo extends JpaRepository<FriendRequest,Long> {
    List<FriendRequest> getFriendRequestByUserAndAcceptedIsFalse(UserEntity user);
    List<FriendRequest> findByUserAndFriend(UserEntity user, UserEntity friend);
    List<FriendRequest> findByUserAndFriendAndAcceptedFalse(UserEntity user, UserEntity friend);
    List<FriendRequest> getFriendRequestByFriendAndAcceptedIsFalse(UserEntity friend);
}
