package de.happybuers.sleepchallange.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

public interface User2FriendshipRepo extends JpaRepository<User2Friendship,Long> {
}
