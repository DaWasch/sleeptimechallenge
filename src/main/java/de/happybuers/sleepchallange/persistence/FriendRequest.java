package de.happybuers.sleepchallange.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;

import jakarta.persistence.*;

@Entity
@Table(schema = "sleep",name = "friend_request_tab")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FriendRequest {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    public UserEntity user;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "friend_id",referencedColumnName = "id")
    public UserEntity friend;
    public Boolean accepted;
}
