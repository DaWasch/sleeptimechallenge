package de.happybuers.sleepchallange.persistence;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.*;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import jakarta.persistence.*;
import java.util.List;
import java.util.Set;

@Entity
@Table(schema = "sleep",name = "user_tab")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class UserEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    private String email;
    private String password;
    private String authorizedClientRegistrationId;
    private String sub;
    @OneToMany(mappedBy = "user",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonManagedReference
    private Set<SleepingTimeEntity> sleepingTimeSet;
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public Set<User2Friendship> user2FriendshipList;
    @OneToMany(mappedBy = "user", fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public Set<FriendRequest> friendRequestsSent;
    @OneToMany(mappedBy = "friend", fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public Set<FriendRequest> friendRequestsReceived;
    @Transient
    private int sumOfPoints;
    @Transient
    private List<SleepingTimeEntity> sleepingTimeList;

}
