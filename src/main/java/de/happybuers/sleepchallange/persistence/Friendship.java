package de.happybuers.sleepchallange.persistence;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import jakarta.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Table(schema = "sleep",name = "friendship_tab")
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class Friendship {
    @Id
    @GeneratedValue
    public Long id;
    @Builder.Default
    public LocalDateTime startDate = LocalDateTime.now();
    @OneToMany(mappedBy = "friendship",fetch = FetchType.EAGER,cascade = CascadeType.REMOVE)
    @OnDelete(action = OnDeleteAction.CASCADE)
    public List<User2Friendship> user2FriendshipList;
}
