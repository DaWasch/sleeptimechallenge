package de.happybuers.sleepchallange.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Optional;

public interface SleepingTimeRepo extends JpaRepository<SleepingTimeEntity,Long> {
    List<SleepingTimeEntity> findByUser(UserEntity user);
    Optional<SleepingTimeEntity> findByUserAndGoToBedTimeBetween(UserEntity user, LocalDateTime goToBedTimeStart, LocalDateTime goToBedTimeEnd);
    SleepingTimeEntity getSleepingTimeEntityById(Long id);
}
