package de.happybuers.sleepchallange.persistence;

import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface UserRepo extends JpaRepository<UserEntity,Long> {
    Optional<UserEntity> findUserEntityByName(String name);
    UserEntity getUserEntityById(Long id);
    Optional<UserEntity> findUserEntityByEmail(String email);
    Optional<UserEntity> findUserEntityByAuthorizedClientRegistrationIdAndSub(String id, String sub);
}
