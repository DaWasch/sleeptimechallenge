package de.happybuers.sleepchallange.persistence;

import com.fasterxml.jackson.annotation.JsonBackReference;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.time.LocalDateTime;

@Entity
@Table(schema = "sleep",name = "sleeping_time_tab")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Getter
public class SleepingTimeEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    public Long id;
    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id",referencedColumnName = "id")
    @JsonBackReference
    public UserEntity user;
    public LocalDateTime goToBedTime;
    public LocalDateTime wakeTime;
    @Transient
    public int points;
}
