# Sleeping Time  Challenge
It's a small web application for challenging your friends to get more sleep than you.

Every time you log your go-to-bed-time you (may) get points

- < 22:00: 3 Points
- < 23:00: 2 Points
- < 00:00: 1 Point
- \>00:00 and < 18:00: 0 Points 

This app uses the OAuth2 login. Currently explicitly implemented is a Login via Google. But for future use any client can be implemented (e.g. https://www.baeldung.com/spring-security-5-oauth2-login)

External postgres is supported. You can activate that via a .env file similar to .env_sample.
As default a H2 Database will be build and persisted under ./h2

Following script starts build and deployment. It also deletes all dangling Docker images. I had no problems with that so far. 

    sh deploy_update.sh

If you don't want to delete old (and unused) images call:

    docker-compose up -d --build

be aware that the build command will generate a new image which will consume disk space!