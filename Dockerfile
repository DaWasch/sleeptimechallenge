FROM eclipse-temurin:21-jre-alpine
WORKDIR /app
RUN addgroup --system javauser && adduser -S -s /bin/false -G javauser javauser

COPY ./target/*.jar ./app.jar

RUN chown -R javauser:javauser /app
USER javauser
ENTRYPOINT java -jar app.jar
